# Terraform

Terraform is an infrastructure-as-code tool that can be used to manage cloud infrastructure. This repository contains instructions to build a Docker image with the following installed:

1. [Terraform](https://www.terraform.io/)
2. [TFLint](https://github.com/terraform-linters/tflint)
3. [Terrascan](https://github.com/accurics/terrascan)

## Development

2. Run `make lint` to lint the Dockerfile
1. Run `make build` to build the image
3. Run `make scan` to run security scans on the built image
4. Run `make test` to run contract tests against the built image
5. Run `make publish` to publish the image

## Continuous Integration

The following variables need to be defined in the CI pipieline:

1. `DOCKER_REGISTRY_URI`: the Docker registry to push to
1. `DOCKER_REGISTRY_USER`: user of the Docker registry to push to
1. `DOCKER_REGISTRY_PASSWORD`: password for the user of the Docker registry to push to
