FROM hashicorp/terraform:0.14.9
SHELL ["/bin/ash", "-eo", "pipefail", "-c"]
# (we need to maintain the terraform version, so ...)
# hadolint ignore=DL3017
RUN apk upgrade --no-cache apk-tools libcrypto1.1 libcurl libssl1.1 ssl_client busybox
RUN apk add --no-cache curl jq make
WORKDIR /tmp

ARG TFLINT_VERSION=0.22.0
RUN curl -Lo ./tflint.zip "https://github.com/terraform-linters/tflint/releases/download/v${TFLINT_VERSION}/tflint_linux_amd64.zip" \
  && unzip ./tflint.zip -d ./ \
  && mv ./tflint /usr/local/bin/tflint \
  && chmod 751 /usr/local/bin/tflint \
  && rm -rf ./tflint.zip

ARG TERRASCAN_VERSION=1.3.3
RUN curl -fL "https://github.com/accurics/terrascan/releases/download/v${TERRASCAN_VERSION}/terrascan_${TERRASCAN_VERSION}_Linux_x86_64.tar.gz" | tar xfz - -C /usr/local/bin \
  && chmod 751 /usr/local/bin/terrascan

ARG TERRAFORM_DOCS_VERSION=v0.10.1
RUN curl -Lo /usr/local/bin/terraform-docs "https://github.com/terraform-docs/terraform-docs/releases/download/${TERRAFORM_DOCS_VERSION}/terraform-docs-${TERRAFORM_DOCS_VERSION}-linux-amd64" \
  && chmod 751 /usr/local/bin/terraform-docs

ENTRYPOINT ["/bin/sh", "-c"]
